import sys
from input import Parser
# from with_knapsack import write_solution, get_videos_by_server
from with_pandas import write_solution, get_videos_by_server


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage:\n\t{} <input_file> <output_file>'.format(sys.argv[0]))
        exit(1)

    input_file = sys.argv[1]
    output_file = sys.argv[2]
    print(input_file, output_file)

    header, videos, network, requests = Parser(input_file).parse()
    print('parsed initial data')
    # solution = get_videos_by_server(0, network, requests, header['X'])
    solution = [get_videos_by_server(c, network, requests, header['X']) for c in range(header['C'])]
    write_solution(output_file, solution)



