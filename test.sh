#!/bin/bash

for i in input/*
do
    echo $i
    out=${i/input/output}
    out=${out/.in/.out}-$(date +%s)
    echo $out

    python solution.py ${i} ${out}
    python scoring.py ${i} ${out}

done
