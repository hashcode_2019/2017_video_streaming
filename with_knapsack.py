import pandas as pd
from knapsack01 import knapsack01


def write_solution(file_path, solution):
    with open(file_path, 'w') as f:
        f.write(str(len(solution)))
        f.write('\n')
        for index, value in enumerate(solution):
            f.write(' '.join(map(str, [index] + value)))
            f.write('\n')


def get_videos_by_server(server: int, network: pd.DataFrame, requests: pd.DataFrame, cache_size: int) -> list:
    this_cache_endpoints: pd.DataFrame = network[network['server'] == server]

    this_cache_endpoints_requests: pd.DataFrame = requests[
        requests['endpoint_id'].isin(this_cache_endpoints['endpoint'])]

    def add_gain(df: pd.DataFrame) -> pd.DataFrame:
        def total_gain_by_endpoint(endpoint, requests_amount):
            return this_cache_endpoints[this_cache_endpoints['endpoint'] == endpoint].iloc[0]['gain'] * requests_amount

        return df.assign(
            total_gain=lambda d: d.apply(lambda row: total_gain_by_endpoint(row['endpoint_id'], row['count']), axis=1))

    this_cache_requests_with_gain: pd.DataFrame = add_gain(this_cache_endpoints_requests)

    def sum_gain_by_video(df: pd.DataFrame) -> pd.DataFrame:
        total_gain_per_video_id = {}
        for _, item in df.iterrows():
            video_id = item['video_id']
            if video_id not in total_gain_per_video_id:
                total_gain_per_video_id[video_id] = {'total_gain': item['total_gain'], 'video_size': item['video_size'],
                                                     'video_id': item['video_id']}
            else:
                total_gain_per_video_id[video_id]['total_gain'] += item['total_gain']

        return pd.DataFrame(total_gain_per_video_id).T

    all_endpoints_gain: pd.DataFrame = sum_gain_by_video(this_cache_requests_with_gain)

    id__tolist = all_endpoints_gain['video_id'].tolist()
    # print(id__tolist)
    size__tolist = all_endpoints_gain['video_size'].tolist()
    # print(size__tolist, len(size__tolist))
    gain__tolist = all_endpoints_gain['total_gain'].tolist()
    # print(gain__tolist, len(gain__tolist))

    result = knapsack01(len(id__tolist), cache_size, size__tolist, gain__tolist)
    # print(result)

    values = list(map(lambda x: id__tolist[x], result))
    # print(values)
    return values
