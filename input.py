import pandas as pd


class Parser:
    def __init__(self, file_path):
        self.cursor = 0
        self.file_path = file_path

    def parse(self):
        header = self.parse_header()
        videos = self.parse_videos()
        network = self.parse_network(header['E'])
        requests = self.parse_requests(header['R'], videos)

        return [header, videos, network, requests]

    def parse_header(self):
        V, E, R, C, X = self.read_next_row().iloc[0]
        return {
            'V': V,
            'E': E,
            'R': R,
            'C': C,
            'X': X
        }

    def parse_videos(self):
        videos = self.read_next_row().transpose()
        videos.columns = ['size']
        return videos

    def parse_network(self, E):
        names = ['server', 'latency']
        network: pd.DataFrame = pd.DataFrame([], columns=['endpoint', 'server', 'latency', 'gain'])
        for e in range(0, E):
            ld, k = self.read_next_row().iloc[0]
            network = network.append({'endpoint': e, 'server': 'datacenter', 'latency': ld, 'gain': 0},
                                     ignore_index=True)
            this_endpoint_network = self.read_rows(k, names)
            network = network.append(
                this_endpoint_network.assign(endpoint=e, gain=lambda df: ld - df['latency']),
                sort=True)

        return network

    def parse_requests(self, R, videos):
        names = ['video_id', 'endpoint_id', 'count']
        requests = self.read_rows(R, names)

        def get_video_size(id: int) -> int:
            return videos.iloc[id]['size']

        requests['video_size'] = requests.apply(lambda x: get_video_size(x['video_id']), axis=1)

        return requests

    def read_rows(self, amount=1, names=None):
        options = dict(sep=' ', skiprows=self.cursor, nrows=amount, header=None)
        if names:
            options.update(names=names)
        rows = pd.read_csv(self.file_path, **options)
        self.cursor += amount
        return rows

    def read_next_row(self):
        return self.read_rows()
