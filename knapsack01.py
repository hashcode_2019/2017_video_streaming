import numpy as np


def knapsack01(n, W, video_sizes, latency_gains):
    ''' Solves the 0/1–knapsack problem in quadratic time and quadratic space using a bottom–up DP approach. '''

    weights, v = np.hstack(([0], video_sizes)), np.hstack(([0], latency_gains))
    dp = np.zeros((n + 1, W + 1), dtype = np.int64)
    for i in range(1, n + 1):
        for j in range(W + 1):
            if weights[i] > j:
                dp[i, j] = dp[i - 1, j]
            else:
                dp[i, j] = max(dp[i - 1, j - weights[i]] + v[i], dp[i - 1, j])

    # Now we need to do the backtracking to come up with the actual optimal configuration
    i, j, videos, total_weight, total_value = n, W, [], 0, 0
    while i > 0 and j > 0:
        while dp[i, j] == dp[i - 1, j] and i >= 0:
            i -= 1

        if i <= 0:
            break

        # Video i must be part of the solution
        videos.append(i - 1)
        total_value += v[i]
        total_weight += weights[i]

        j -= weights[i]
        i -= 1

    assert len(videos) == len(set(videos)), 'Invalid solution: Items should only appear at most once in the cache.'
    assert total_value == dp[-1, -1], 'Invalid solution: Optimal total value should be %d, but is %d instead.' % (dp[-1, -1], total_value)
    assert total_weight <= W, 'Invalid solution: Total weight of items can not exceed cache capacity...'

    return videos

